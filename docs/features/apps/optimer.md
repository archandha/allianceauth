# Fleet Operations

Fleet Operations is an app for organizing and communicating fleet schedules.

![optimer](/_static/images/features/apps/optimer.png)

## Installation

- Add `'allianceauth.optimer',` to `INSTALLED_APPS` in your `local.py`

Perform Django Maintenance and restart our Web Service and Workers.

::::{tabs}
:::{group-tab} Bare Metal

```shell
python manage.py migrate
python manage.py collectstatic --noinput
supervisorctl restart myauth:
```

:::
:::{group-tab} Containerized

```shell
docker compose --env-file=.env up -d
docker compose exec allianceauth_gunicorn bash
auth migrate
auth collectstatic
```

:::
::::

## Permissions

To use and administer this feature, users will require some of the following.

```{eval-rst}
+---------------------------------------+------------------+--------------------------------------------------------------------------+
| Permission                            | Admin Site       | Auth Site                                                                |
+=======================================+==================+==========================================================================+
| auth.optimer_view                     | None             | Can view Fleet Operation Timers                                          |
+---------------------------------------+------------------+--------------------------------------------------------------------------+
| auth.optimer_manage                   | None             | Can Manage Fleet Operation timers                                        |
+---------------------------------------+------------------+--------------------------------------------------------------------------+
```
