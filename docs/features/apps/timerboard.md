# Structure Timers

Structure Timers helps you keep track of both offensive and defensive structure timers in your space.

![timerboard](/_static/images/features/apps/timerboard.png)

## Installation

- Add `'allianceauth.timerboard',` to `INSTALLED_APPS` in your `local.py`

Perform Django Maintenance and restart our Web Service and Workers.

::::{tabs}
:::{group-tab} Bare Metal

```shell
python manage.py migrate
python manage.py collectstatic --noinput
supervisorctl restart myauth:
```

:::
:::{group-tab} Containerized

```shell
docker compose --env-file=.env up -d
docker compose exec allianceauth_gunicorn bash
auth migrate
auth collectstatic
```

:::
::::

## Permissions

To use and administer this feature, users will require some of the following.

```{eval-rst}
+---------------------------------------+------------------+--------------------------------------------------------------------------+
| Permission                            | Admin Site       | Auth Site                                                                |
+=======================================+==================+==========================================================================+
| auth.timer_view                       | None             | Can view Timerboard Timers                                               |
+---------------------------------------+------------------+--------------------------------------------------------------------------+
| auth.timer_manage                     | None             | Can Manage Timerboard timers                                             |
+---------------------------------------+------------------+--------------------------------------------------------------------------+
```
